let trainer = {
	name: "Ash Ketchum",
	age: 10,
	friends: {
		hoenn:	["May", "Max"],
		kanto: ["Brock", "Bulbasaur"], 
	},
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	talk: function(){
		console.log("Pikachu, I choose you!");
	}
}
console.log(trainer);
trainer.talk();
console.log(typeof trainer);


function Pokemon(name, level, health) {

    // Properties
    this.name = name;
    this.level = level;
    this.health = health;
    this.attack = level;
    let pokemonDamage1 = 
    this.tackle = function(target) {
	    	let pokemonDamage = target.health - this.attack;
	        console.log(this.name + ' tackled ' + target.name);
	        console.log(target.name + "'s health is now reduced to " + pokemonDamage);
         	let pikachu2 = new Pokemon(target.name, target.level, pokemonDamage);
         	console.log(pikachu2);
        };

    this.faint = function(target){
       
        let pokemonDamage = target.health - this.attack;
        console.log(this.name + ' tackled ' + target.name);
        console.log(target.name + "'s health is now reduced to " + pokemonDamage);
        if (pokemonDamage <= 0) {
 				console.log(target.name + ' fainted.');
        }
        let geodude2 = new Pokemon(target.name, target.level, pokemonDamage);
         	console.log(geodude2);
        
	}

}


let pikachu = new Pokemon("Pikachu", 16, 32);
console.log(pikachu);
let geodude = new Pokemon('Geodude', 8, 16);
console.log(geodude);
let mewtwo = new Pokemon('Mewtwo', 100, 200);
console.log(mewtwo);




geodude.tackle(pikachu);
mewtwo.faint(geodude);